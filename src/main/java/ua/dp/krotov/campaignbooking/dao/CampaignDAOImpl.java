package ua.dp.krotov.campaignbooking.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.dp.krotov.campaignbooking.model.*;

import java.sql.*;
import java.util.List;
import java.util.Optional;

import static ua.dp.krotov.campaignbooking.util.Converter.convertDateToSql;

@Slf4j
@Repository
public class CampaignDAOImpl implements CampaignDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Optional<Campaign> get(Integer id) {
        String query = "SELECT * FROM CAMPAIGNS WHERE ID=?";
        try {
            Campaign campaign = jdbcTemplate.queryForObject(query, new Object[]{id}, new CampaignRowMapper());
            return Optional.of(campaign);
        }  catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Campaign post(Campaign campaign) {

        String query = "INSERT INTO CAMPAIGNS(NAME, START_DATE, END_DATE) VALUES (?, ?, ?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {

            PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, campaign.getName());
            ps.setDate(2, convertDateToSql(campaign.getStartDate()));
            ps.setDate(3, convertDateToSql(campaign.getEndDate()));
            return ps;
        }, keyHolder);

        campaign.setId(keyHolder.getKey().intValue());
        return campaign;
    }

    @Override
    public Campaign put(Integer id, Campaign campaign) {
        String query = "UPDATE CAMPAIGNS SET NAME = ?, STATUS = ?, START_DATE = ?, END_DATE = ? WHERE ID = ?";

        jdbcTemplate.update(connection -> {

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, campaign.getName());
            ps.setInt(2, campaign.getStatus().getValue());
            ps.setDate(3, convertDateToSql(campaign.getStartDate()));
            ps.setDate(4, convertDateToSql(campaign.getEndDate()));
            ps.setInt(5, id);
            return ps; });

        campaign.setId(id);
        return campaign;
    }

    @Override
    public Integer delete(Integer id) {
        log.debug("DELETE CAMPAIGN WITH ID: [{}]", id);
        String query = "DELETE FROM CAMPAIGNS WHERE ID = (SELECT CAMPAIGNS.ID FROM CAMPAIGNS WHERE ID = ?)";
        return jdbcTemplate.update(connection -> {

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            return ps; });
    }

    @Override
    public List<CampaignSummary> getSummaries(String query, Page page, Sort sort) {

        String sqlQuery = "SELECT CAMPAIGNS.ID, CAMPAIGNS.NAME, CAMPAIGNS.STATUS, COUNT(ADS.ID) AS ADS_COUNT FROM CAMPAIGNS" +
                " LEFT JOIN ADS ON ADS.FK_CAMPAIGN_ID = CAMPAIGNS.ID WHERE " +
                " ROWNUM BETWEEN " + page.getOffset() +" AND " + page.getLimit() + " AND CAMPAIGNS.NAME LIKE '%" + query + "%'" +
                " GROUP BY CAMPAIGNS.ID ORDER BY " + sort.getOrderBy() + " " + sort.getDirection();

        return jdbcTemplate.query(
                sqlQuery,
                (rs, rowNum) -> CampaignSummary.builder()
                        .id(rs.getInt("ID"))
                        .name(rs.getString("NAME"))
                        .status(Status.values()[rs.getInt("STATUS")])
                        .adsCount(rs.getInt("ADS_COUNT"))
                        .build());
    }

    private class CampaignRowMapper implements RowMapper<Campaign> {

        @Override
        public Campaign mapRow(ResultSet rs, int rowNum) throws SQLException {

            return Campaign.builder()
                    .id(rs.getInt("ID"))
                    .name(rs.getString("NAME"))
                    .status(Status.values()[rs.getInt("STATUS")])
                    .startDate(rs.getDate("START_DATE"))
                    .endDate(rs.getDate("END_DATE"))
                    .build();
        }
    }
}
