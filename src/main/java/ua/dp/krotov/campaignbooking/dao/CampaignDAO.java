package ua.dp.krotov.campaignbooking.dao;

import ua.dp.krotov.campaignbooking.model.*;

import java.util.List;
import java.util.Optional;

public interface CampaignDAO {

    Optional<Campaign> get(Integer id);

    Campaign post(Campaign campaign);

    Campaign put(Integer id, Campaign campaign);

    Integer delete(Integer id);

    List<CampaignSummary> getSummaries(String query, Page page, Sort sort);
}
