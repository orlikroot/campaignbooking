package ua.dp.krotov.campaignbooking.dao;

import ua.dp.krotov.campaignbooking.model.Ad;

import java.util.Optional;

public interface AdDAO {

    Optional<Ad> get(Integer id);

    Ad post(Ad ad);

    Ad put(Integer id, Ad ad);

    Integer delete(Integer id);
}
