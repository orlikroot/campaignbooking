package ua.dp.krotov.campaignbooking.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;
import ua.dp.krotov.campaignbooking.common.NotFoundException;
import ua.dp.krotov.campaignbooking.model.Ad;
import ua.dp.krotov.campaignbooking.model.Platform;
import ua.dp.krotov.campaignbooking.model.Status;

import java.sql.*;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Repository
public class AdDAOImpl implements AdDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional(readOnly = true)
    @Override
    public Optional<Ad> get(Integer id) {

        String query = "SELECT * FROM ADS WHERE ID=?";
        try {
            Ad ad = jdbcTemplate.queryForObject(query, new Object[]{id}, new AdRowMapper());
            return Optional.of(ad);
        }  catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Ad post(Ad ad) {

        String query = "INSERT INTO ADS(FK_CAMPAIGN_ID, NAME, STATUS, PLATFORMS, ASSET_URL) VALUES (?, ?, ?, ?, ?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {

            Array platforms = connection.createArrayOf("VARCHAR", ad.getPlatforms().stream().map(Platform::getValue).collect(Collectors.toList()).toArray());

            PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, ad.getCampaignId());
            ps.setString(2, ad.getName());
            ps.setInt(3, ad.getStatus().getValue());
            ps.setArray(4, platforms);
            ps.setString(5, ad.getAssetUrl());
            return ps;
        }, keyHolder);

        ad.setId(keyHolder.getKey().intValue());
        return ad;
    }

    @Override
    public Ad put(Integer id, Ad ad) {

        String query = "UPDATE ADS SET FK_CAMPAIGN_ID = ?, NAME = ?, STATUS = ?, PLATFORMS = ?, ASSET_URL = ? WHERE ID = ?";

        jdbcTemplate.update(connection -> {

            Array platforms = connection.createArrayOf("VARCHAR", ad.getPlatforms().stream().map(Platform::getValue).collect(Collectors.toList()).toArray());

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, ad.getCampaignId());
            ps.setString(2, ad.getName());
            ps.setInt(3, ad.getStatus().getValue());
            ps.setArray(4, platforms);
            ps.setString(5, ad.getAssetUrl());
            ps.setInt(6, id);
            return ps; });

        ad.setId(id);
        return ad;
    }

    @Override
    public Integer delete(Integer id) {

            String query = "DELETE FROM ADS WHERE ID = ?";
            return jdbcTemplate.update(connection -> {

                PreparedStatement ps = connection.prepareStatement(query);
                ps.setInt(1, id);
                return ps; });

    }

    private class AdRowMapper implements RowMapper<Ad> {

        @Override
        public Ad mapRow(ResultSet rs, int rowNum) throws SQLException {
            Array platforms = rs.getArray("PLATFORMS");
            Object[] array = (Object[]) platforms.getArray();
            List<Platform> platformList = Arrays.stream(array)
                    .map(pf -> Platform.getByValue(Integer.parseInt(pf.toString())))
                    .collect(Collectors.toList());

            return Ad.builder()
                    .id(rs.getInt("ID"))
                    .name(rs.getString("NAME"))
                    .status(Status.values()[rs.getInt("STATUS")])
                    .assetUrl(rs.getString("ASSET_URL"))
                    .platforms(platformList)
            .build();
        }
    }
}
