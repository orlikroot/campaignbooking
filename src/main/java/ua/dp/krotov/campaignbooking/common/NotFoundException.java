package ua.dp.krotov.campaignbooking.common;

public class NotFoundException extends Exception {
    public NotFoundException(String message) {
        super(message);
    }
}
