package ua.dp.krotov.campaignbooking.common;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

@Data
public class ApiError {

    private HttpStatus status;
    private ZonedDateTime timestamp;
    private String message;
    private List<String> errors;

    public ApiError(HttpStatus status, String message, List<String> errors) {
        super();
        this.status = status;
        this.timestamp = ZonedDateTime.now();
        this.message = message;
        this.errors = errors;
    }

    public ApiError(HttpStatus status, String message, String error) {
        super();
        this.status = status;
        this.timestamp = ZonedDateTime.now();
        this.message = message;
        errors = Arrays.asList(error);
    }
}
