package ua.dp.krotov.campaignbooking.util;

public class Converter {

    public static java.sql.Date convertDateToSql(java.util.Date uDate) {
        java.sql.Date sDate = new java.sql.Date(uDate.getTime());
        return sDate;
    }
}
