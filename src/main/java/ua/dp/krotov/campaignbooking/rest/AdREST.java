package ua.dp.krotov.campaignbooking.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.dp.krotov.campaignbooking.common.NotFoundException;
import ua.dp.krotov.campaignbooking.model.Ad;
import ua.dp.krotov.campaignbooking.service.AdService;

@RestController
@RequestMapping("/ad")
public class AdREST {

    @Autowired
    private AdService adService;

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Ad get(final @PathVariable Integer id) throws NotFoundException {
        return adService.get(id);
    }

    @RequestMapping(path = "/", method = RequestMethod.POST)
    @ResponseBody
    public Ad post(final @RequestBody Ad ad) {
        return adService.post(ad);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public Ad put(final @PathVariable Integer id,
                  final @RequestBody Ad ad) {
        return adService.put(id, ad);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(final @PathVariable Integer id) throws NotFoundException {
        adService.delete(id);
    }
}
