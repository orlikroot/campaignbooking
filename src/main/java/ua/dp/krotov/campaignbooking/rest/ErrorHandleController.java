package ua.dp.krotov.campaignbooking.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import springfox.documentation.annotations.ApiIgnore;
import ua.dp.krotov.campaignbooking.common.NotFoundException;
import ua.dp.krotov.campaignbooking.common.ApiError;

import java.sql.SQLException;

@ControllerAdvice
@ApiIgnore
public class ErrorHandleController {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity handleException(NotFoundException exception) {
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, exception.getMessage(), "NOT FOUND");

        return new ResponseEntity<Object>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity handleException(IllegalArgumentException exception) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, exception.getMessage(), "VALIDATION ERROR");

        return new ResponseEntity<Object>(apiError, apiError.getStatus());
    }


    //TODO Add proper error handler for SQLException, Exception, etc.
    @ExceptionHandler(SQLException.class)
    public ResponseEntity handleException(SQLException exception) {
        ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, exception.getMessage(), "SERVER ERROR");

        return new ResponseEntity<Object>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception exception) {
        ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, exception.getMessage(), "SERVER ERROR");

        return new ResponseEntity<Object>(apiError, apiError.getStatus());
    }
}
