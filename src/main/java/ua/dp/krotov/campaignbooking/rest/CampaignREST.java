package ua.dp.krotov.campaignbooking.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.dp.krotov.campaignbooking.common.NotFoundException;
import ua.dp.krotov.campaignbooking.model.*;
import ua.dp.krotov.campaignbooking.service.CampaignService;

import java.util.List;

@RestController
@RequestMapping("/")
public class CampaignREST {

    @Autowired
    private CampaignService campaignService;

    @RequestMapping(path = "/summaries", method = RequestMethod.GET)
    @ResponseBody
    public List<CampaignSummary> summaries(final @RequestParam String query,
                                            final @RequestParam(defaultValue = "10") Integer limit,
                                            final @RequestParam(defaultValue = "0") Integer offset,
                                            final @RequestParam(defaultValue = "ID") String orderBy,
                                            final @RequestParam(defaultValue = "ASC") String dir) {

        Page page = Page.builder()
                .limit(limit)
                .offset(offset)
                .build();

        String validOrder = Order.valueOf(orderBy.toUpperCase()).name();
        String validDirection = Direction.valueOf(dir.toUpperCase()).name();

        Sort sort = Sort.builder()
                .orderBy(validOrder)
                .direction(validDirection)
                .build();

        return campaignService.getSummaries(query, page, sort);
    }

    @RequestMapping(path = "/campaign/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Campaign get(final @PathVariable Integer id) throws NotFoundException {
        return campaignService.get(id);
    }

    @RequestMapping(path = "/campaign", method = RequestMethod.POST)
    @ResponseBody
    public Campaign post(final @RequestBody Campaign campaign) {
        return campaignService.post(campaign);
    }

    @RequestMapping(path = "/campaign/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public Campaign put(final @PathVariable Integer id,
                        final @RequestBody Campaign campaign) {
        return campaignService.put(id, campaign);
    }

    @RequestMapping(path = "/campaign/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(final @PathVariable Integer id) throws NotFoundException {
        campaignService.delete(id);
    }
}
