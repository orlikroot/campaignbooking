package ua.dp.krotov.campaignbooking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.krotov.campaignbooking.common.NotFoundException;
import ua.dp.krotov.campaignbooking.dao.CampaignDAO;
import ua.dp.krotov.campaignbooking.model.*;

import java.util.List;

@Service
public class CampaignServiceImpl implements CampaignService {

    @Autowired
    private CampaignDAO campaignDAO;

    @Override
    public Campaign get(Integer id) throws NotFoundException {
        return campaignDAO.get(id).orElseThrow(() -> new NotFoundException(String.format("Campaign with id: [%d] not found.", id)));
    }

    @Override
    public Campaign post(Campaign campaign) {
        return campaignDAO.post(campaign);
    }

    @Override
    public Campaign put(Integer id, Campaign campaign) {
        return campaignDAO.put(id, campaign);
    }

    @Override
    public void delete(Integer id) throws NotFoundException {
        Integer rows = campaignDAO.delete(id);
        if (rows == 0) throw new NotFoundException(String.format("Campaign with id: [%d] not found.", id));
    }

    @Override
    public List<CampaignSummary> getSummaries(String query, Page page, Sort sort) {
        return campaignDAO.getSummaries(query, page, sort);
    }
}
