package ua.dp.krotov.campaignbooking.service;

import ua.dp.krotov.campaignbooking.common.NotFoundException;
import ua.dp.krotov.campaignbooking.model.*;

import java.util.List;

public interface CampaignService {

    Campaign get(Integer id) throws NotFoundException;

    Campaign post(Campaign campaign);

    Campaign put(Integer id, Campaign campaign);

    void delete(Integer id) throws NotFoundException;

    List<CampaignSummary> getSummaries(String query, Page page, Sort sort);
}
