package ua.dp.krotov.campaignbooking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.krotov.campaignbooking.dao.AdDAO;
import ua.dp.krotov.campaignbooking.common.NotFoundException;
import ua.dp.krotov.campaignbooking.model.Ad;

@Service
public class AdServiceImpl implements AdService {

    @Autowired
    private AdDAO adDAO;

    @Override
    public Ad get(Integer id) throws NotFoundException {
        return adDAO.get(id).orElseThrow(() -> new NotFoundException(String.format("Ad with id: [%d] not found.", id)));
    }

    @Override
    public Ad post(Ad ad) {
        return adDAO.post(ad);
    }

    @Override
    public Ad put(Integer id, Ad ad) {
        return adDAO.put(id, ad);
    }

    @Override
    public void delete(Integer id) throws NotFoundException {
       Integer rows =  adDAO.delete(id);
        if (rows == 0) throw new NotFoundException(String.format("Ad with id: [%d] not found.", id));
    }
}
