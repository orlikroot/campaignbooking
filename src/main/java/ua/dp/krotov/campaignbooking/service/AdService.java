package ua.dp.krotov.campaignbooking.service;

import ua.dp.krotov.campaignbooking.common.NotFoundException;
import ua.dp.krotov.campaignbooking.model.Ad;

public interface AdService {

    Ad get(Integer id) throws NotFoundException;

    Ad post(Ad ad);

    Ad put(Integer id, Ad ad);

    void delete(Integer id) throws NotFoundException;
}
