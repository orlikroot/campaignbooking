package ua.dp.krotov.campaignbooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CampaignbookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CampaignbookingApplication.class, args);
	}

}
