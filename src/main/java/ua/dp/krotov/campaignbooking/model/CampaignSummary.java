package ua.dp.krotov.campaignbooking.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CampaignSummary {

    private Integer id;
    private String name;
    private Status status;
    private Integer adsCount;
}
