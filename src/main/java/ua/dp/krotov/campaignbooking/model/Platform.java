package ua.dp.krotov.campaignbooking.model;

import java.util.HashMap;
import java.util.Map;

public enum Platform {
    WEB (0),
    ANDROID (1),
    IOS (2);

    private static final Map<Integer, Platform> ENUM_MAP = new HashMap<Integer, Platform>();
    static {
        for (Platform platform : values()) {
            ENUM_MAP.put(platform.getValue(), platform);
        }
    }
    private int value;

    private Platform(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Platform getByValue(int value) {
        return ENUM_MAP.get(value);
    }

    public String toString() {
        return name() + "=" + value;
    }
}
