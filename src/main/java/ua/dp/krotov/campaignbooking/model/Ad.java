package ua.dp.krotov.campaignbooking.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Ad {

    private Integer id;
    private String name;
    private Status status;
    private List<Platform> platforms;
    private String assetUrl;
    private Integer campaignId;
}
