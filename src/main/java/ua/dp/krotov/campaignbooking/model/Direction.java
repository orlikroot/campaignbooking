package ua.dp.krotov.campaignbooking.model;

public enum Direction {
    ASC,
    DESC
}
