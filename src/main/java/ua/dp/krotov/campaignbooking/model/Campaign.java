package ua.dp.krotov.campaignbooking.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Campaign {

    private Integer id;
    private String name;
    private Status status;
    private Date startDate;
    private Date endDate;
}
