package ua.dp.krotov.campaignbooking.model;

import java.util.HashMap;
import java.util.Map;

public enum Status {
    PLANNED (0),
    ACTIVE (1),
    PAUSED (2),
    FINISHED (3);

    private static final Map<Integer, Status> ENUM_MAP = new HashMap<Integer, Status>();
    static {
        for (Status status : values()) {
            ENUM_MAP.put(status.getValue(), status);
        }
    }
    private int value;

    private Status(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Status getByValue(int value) {
        return ENUM_MAP.get(value);
    }

    public String toString() {
        return name() + "=" + value;
    }
}
